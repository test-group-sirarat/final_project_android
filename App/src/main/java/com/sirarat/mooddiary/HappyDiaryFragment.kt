package com.sirarat.mooddiary

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.utils.widget.ImageFilterButton
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.sirarat.mooddiary.data.DiaryHappy
import com.sirarat.mooddiary.databinding.FragmentHappyDiaryBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HappyDiaryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HappyDiaryFragment : Fragment() {

    private val viewModel: HappyDiaryViewModel by activityViewModels {
        HappyViewModelFactory(
            (activity?.application as HappyApplication).database.HappyDao()
        )
    }

    lateinit var diaryHappy: DiaryHappy


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentHappyDiaryBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHappyDiaryBinding.inflate(inflater,container,false)
        return binding?.root
    }
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//
//    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Hide keyboard.
        val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as
                InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus?.windowToken, 0)
        _binding = null


    }

    private fun isEntryValid(): Boolean {
        return viewModel.isEntryValid(
            binding?.TitleDiaryHappy?.text.toString(),
            binding?.descriptionDiaryHappy?.text.toString()
        )
    }

    private fun addHappy() {
        if (isEntryValid()) {
            viewModel.addNewHappy(
                binding?.TitleDiaryHappy?.text.toString(),
                binding?.descriptionDiaryHappy?.text.toString()
            )
            val action = HappyDiaryFragmentDirections.actionHappyDiaryFragmentToHappyListFragment()
            findNavController().navigate(action)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.buttonSaveHappy?.setOnClickListener {
            addHappy()
        }
    }
}

