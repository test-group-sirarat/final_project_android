package com.sirarat.mooddiary

import androidx.lifecycle.*
import com.sirarat.mooddiary.data.DiaryHappy
import com.sirarat.mooddiary.data.HappyDao
import kotlinx.coroutines.launch

class HappyDiaryViewModel(private val happyDao: HappyDao) : ViewModel() {

    val allItems: LiveData<List<DiaryHappy>> = happyDao.getItems().asLiveData()

    private fun insertHappy(diaryHappy: DiaryHappy) {
        viewModelScope.launch {
            happyDao.insert(diaryHappy)
        }
    }

    private fun getNewItemEntry( titleHappy: String, descriptionHappy: String): DiaryHappy {
        return DiaryHappy(
            DiaryTitleHappy =  titleHappy,
            diarydescriptionHappy = descriptionHappy
        )
    }

    fun addNewHappy(titleHappy: String, descriptionHappy: String) {
        val newHappy = getNewItemEntry(titleHappy, descriptionHappy)
        insertHappy(newHappy)
    }

    fun isEntryValid(titleDiaryHappy: String,descriptionDiaryHappy: String): Boolean {
        if (titleDiaryHappy.isBlank() || descriptionDiaryHappy.isBlank()) {
            return false
        }
        return true
    }
    fun deleteHappy(diaryHappy: DiaryHappy) {
       viewModelScope.launch {
            happyDao.delete(diaryHappy)
       }
   }

    fun Item(id: Int): LiveData<DiaryHappy> {
        return happyDao.getItem(id).asLiveData()
    }

}

class HappyViewModelFactory(private val happyDao: HappyDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HappyDiaryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HappyDiaryViewModel(happyDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}