package com.sirarat.mooddiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sirarat.mooddiary.data.DiaryHappy
import com.sirarat.mooddiary.databinding.FragmentHappyDetailBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HappyDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HappyDetailFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private val navigationArgs: HappyDetailFragmentArgs by navArgs()
    lateinit var diaryHappy: DiaryHappy
    private var _binding: FragmentHappyDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
        }
    }
    private val viewModel: HappyDiaryViewModel by activityViewModels {
        HappyViewModelFactory(
            (activity?.application as HappyApplication).database.HappyDao()
        )
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHappyDetailBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        viewModel.Item(id).observe(this.viewLifecycleOwner)
        { selectedItem ->
            diaryHappy = selectedItem
            bind(diaryHappy)
        }

    }
    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.comfirmNo)) { _, _ -> }
            .setPositiveButton(getString(R.string.comfirmYes)) { _, _ ->
                deleteHappy()
            }
            .show()
    }

    private fun deleteHappy() {
        viewModel.deleteHappy(diaryHappy)
        findNavController().navigateUp()
    }

    private fun bind(diaryHappy: DiaryHappy) {
        binding.apply {
            TitleDetailHappy.text = diaryHappy.DiaryTitleHappy
            DescriptionDetailHappy.text = diaryHappy.diarydescriptionHappy
            buttonDeleteHappy.setOnClickListener { showConfirmationDialog() }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}


