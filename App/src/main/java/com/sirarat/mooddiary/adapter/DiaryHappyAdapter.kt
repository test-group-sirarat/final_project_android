package com.sirarat.mooddiary.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sirarat.mooddiary.data.DiaryHappy
import com.sirarat.mooddiary.databinding.HappyListHappyBinding

class DiaryHappyAdapter (private val onItemClicked: (DiaryHappy) -> Unit) :
    ListAdapter<DiaryHappy, DiaryHappyAdapter.DiaryHappyViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiaryHappyViewHolder {
        return DiaryHappyViewHolder(
            HappyListHappyBinding.inflate(
                LayoutInflater.from(
                    parent.context
                )
            )
        )
    }

    override fun onBindViewHolder(holder: DiaryHappyViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onItemClicked(current)
        }
        holder.bind(current)
    }

    class DiaryHappyViewHolder(private var binding: HappyListHappyBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(diaryHappy: DiaryHappy) {
            binding.TiTleHappy.text = diaryHappy.DiaryTitleHappy.toString()

        }
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<DiaryHappy>() {
            override fun areItemsTheSame(oldItem: DiaryHappy, newItem: DiaryHappy): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: DiaryHappy, newItem:DiaryHappy): Boolean {
                return oldItem.diarydescriptionHappy == newItem.diarydescriptionHappy
            }
        }
    }
}