/*
 * Copyright (C) 2021 The Android Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sirarat.mooddiary.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow


/**
 * Database access object to access the Inventory database
 */
@Dao
interface HappyDao {

    @Query("SELECT * from diaryHappy ORDER BY description ASC")
    fun getItems(): Flow<List<DiaryHappy>>
    @Query("SELECT * from diaryHappy WHERE diaryHappy_id = :id")
    fun getItem(id: Int): Flow<DiaryHappy>

    @Insert
    suspend fun insert(diaryHappy: DiaryHappy)
    @Update
    suspend fun update(diaryHappy: DiaryHappy)
    @Delete
    suspend fun delete(diaryHappy: DiaryHappy)
}
