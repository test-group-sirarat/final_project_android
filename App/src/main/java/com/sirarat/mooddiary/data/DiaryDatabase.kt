package com.sirarat.mooddiary.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(DiaryHappy:: class) , version = 1, exportSchema = false)
abstract class DiaryDatabase :RoomDatabase(){


    abstract fun HappyDao(): HappyDao

    companion object{
        @Volatile
        private var INSTANCE: DiaryDatabase? = null
        fun getDatabase(context: Context): DiaryDatabase {
            return INSTANCE ?: synchronized(this){
                val instant = Room.databaseBuilder(
                    context.applicationContext,
                    DiaryDatabase::class.java,
                    "diary_database"
                ).build()
                INSTANCE = instant
                instant
            }
        }

    }
}