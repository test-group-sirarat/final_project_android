package com.sirarat.mooddiary.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class DiaryHappy
    (
    @PrimaryKey(autoGenerate = true)
    val diaryHappy_id: Int = 0,
    @ColumnInfo(name = "title") val DiaryTitleHappy: String ,
    @ColumnInfo(name = "description")val diarydescriptionHappy :String
     )
