package com.sirarat.mooddiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.sirarat.mooddiary.adapter.DiaryHappyAdapter
import com.sirarat.mooddiary.databinding.FragmentHappyDiaryBinding
import com.sirarat.mooddiary.databinding.FragmentHappyListBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HappyListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HappyListFragment : Fragment() {

    private val viewModel: HappyDiaryViewModel by activityViewModels {
        HappyViewModelFactory(
            (activity?.application as HappyApplication).database.HappyDao()
        )
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentHappyListBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHappyListBinding.inflate(inflater, container, false)
        return binding?.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = DiaryHappyAdapter{
            val action =
                HappyListFragmentDirections.actionHappyListFragmentToHappyDetailFragment(it.diaryHappy_id)
            this.findNavController().navigate(action)
        }
        binding?.HappyRecyclerView?.adapter = adapter
        binding?.buttonAddHappy?.setOnClickListener {
            val action = HappyListFragmentDirections.actionHappyListFragmentToHappyDiaryFragment()
            view.findNavController().navigate(action)
        }
        binding?.HappyRecyclerView?.layoutManager = LinearLayoutManager(this.context)

        viewModel.allItems.observe(this.viewLifecycleOwner) { items ->
            items.let {
                adapter.submitList(it)
            }
        }
        binding?.buttonBackToHome?.setOnClickListener {
            val action = HappyListFragmentDirections.actionHappyListFragmentToMainFragment()
            view.findNavController().navigate(action)
        }

    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}

